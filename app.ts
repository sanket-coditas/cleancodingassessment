import express from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs/promises';
import { endPoints, messages } from './utility/constants';

const app = express();

const PORT = process.env.PORT;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});


const storage = multer.diskStorage({
    destination: './uploads/'
});

const upload = multer({ storage });

app.post(endPoints.upload, upload.single('file'), (req, res) => {
    res.status(200).json({ message: messages.uploadSuccess });
});




app.get(endPoints.retrive, (req, res) => {
    const filename = req.params.filename;
    const filePath = path.join(__dirname, 'uploads', filename);
    res.sendFile(filePath);
});


app.delete(endPoints.delete, async (req, res) => {
    const filename = req.params.filename;
    const filePath = path.join(__dirname, 'uploads', filename);
    try {
        await fs.unlink(filePath);
        res.status(200).json({ message: messages.deleteSuccess });
    } catch (error) {
        res.status(500).json({ error: 'Error deleting file.' });
    }
});


