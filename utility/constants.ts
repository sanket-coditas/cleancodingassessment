export const endPoints = {
    upload: '/api/upload',
    retrive: '/api/retrieve/:filename',
    delete: '/api/delete/:filename'
}

export const messages = {
    uploadSuccess: 'File uploaded successfully.',
    deleteSuccess: 'File deleted successfully.',
    
    
}
