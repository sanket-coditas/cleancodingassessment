"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const promises_1 = __importDefault(require("fs/promises"));
const constants_1 = require("./utility/constants");
const app = (0, express_1.default)();
const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
const storage = multer_1.default.diskStorage({
    destination: './uploads/',
    filename: (req, file, callback) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        callback(null, file.fieldname + '-' + uniqueSuffix + path_1.default.extname(file.originalname));
    },
});
const upload = (0, multer_1.default)({ storage });
app.post(constants_1.endPoints.upload, upload.single('file'), (req, res) => {
    res.status(200).json({ message: constants_1.messages.uploadSuccess });
});
app.get(constants_1.endPoints.retrive, (req, res) => {
    const filename = req.params.filename;
    const filePath = path_1.default.join(__dirname, 'uploads', filename);
    res.sendFile(filePath);
});
app.delete(constants_1.endPoints.delete, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const filename = req.params.filename;
    const filePath = path_1.default.join(__dirname, 'uploads', filename);
    try {
        yield promises_1.default.unlink(filePath);
        res.status(200).json({ message: 'File deleted successfully.' });
    }
    catch (error) {
        res.status(500).json({ error: 'Error deleting file.' });
    }
}));
//# sourceMappingURL=app.js.map