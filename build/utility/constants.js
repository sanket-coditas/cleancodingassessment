"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.messages = exports.endPoints = void 0;
exports.endPoints = {
    upload: '/api/upload',
    retrive: '/api/retrieve/:filename',
    delete: '/api/delete/:filename'
};
exports.messages = {
    uploadSuccess: 'File uploaded successfully.'
};
//# sourceMappingURL=constants.js.map